# Formation Kubernetes du 17/04/2023

## TP 1 - Docker

## TP 2 - Pods

## TP 3 - Nodes, Namespaces

## TP 4 - ReplicationController, ReplicaSet

## TP 5 - Deployment

## TP 6, 7 - Services, Ingress

## TP 8 - Command & arguments, Env. variables

## TP 9 - Liveness probe, Readiness probe

## TP 10 - Volumes (emptyDir)

## TP 11 - Volumes (PersistentVolume, PersistentVolumeClaim)

## TP 12 - ConfigMap, Secret

## TP 13 - Jobs, Cronjobs
