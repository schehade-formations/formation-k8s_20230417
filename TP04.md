# TP 4 - ReplicationController, ReplicaSet

```bash
mkdir ../TP04 && cd ../TP04
vim my-rc.yaml
```

my-rc.yaml

```yaml
apiVersion: v1
kind: ReplicationController
metadata:
  name: node-server
spec:
  replicas: 2
  selector:
    app: backend
  template:
    metadata:
      labels:
        app: backend
    spec:
      containers:
      - name: node-server-ctn
        image: <my-username>/formation-k8s-my-node-server:1.0.0
        ports:
        - containerPort: 8080
```

```bash
kubectl delete pods my-pod my-pod-2
# Dans un 2nd terminal
kubectl get pods -w
# On revient dans le terminal original
kubectl apply -f my-rc.yaml
kubectl get replicationcontrollers
kubectl delete pod node-server-<???>

cp my-rc.yaml my-rs.yaml
vim my-rs.yaml
```

my-rs.yaml

```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: node-server
spec:
  replicas: 2
  selector:
    matchLabels:
      app: backend-2
  template:
    metadata:
      labels:
        app: backend-2
    spec:
      containers:
      - name: node-server-ctn
        image: <my-username>/formation-k8s-my-node-server:1.0.0
        ports:
        - containerPort: 8080
```

```bash
kubectl apply -f my-rs.yaml
kubectl get pods
kubectl get replicasets
```
