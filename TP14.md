# TP 14

```bash
kubectl describe node minikube | less
mkdir ../TP14 && cd ../TP14
kubectl run my-pod --image=samiche92/my-node-server:1.0.0 --port=8080 -o yaml --dry-run=client > my-pod.yaml
vim my-pod.yaml
```

my-pod.yaml

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod
spec:
  containers:
  - image: samiche92/my-node-server:1.0.0
    name: my-pod
    ports:
    - containerPort: 8080
    resources:
      requests:
        memory: "100Mi"
        cpu: "3"
```

```bash
kubectl apply -f my-pod.yaml
kubectl get pods
kubectl describe pod my-db | grep QoS
kubectl describe pod my-pod | grep QoS

cp my-pod.yaml my-pod-2.yaml 
vim my-pod-2.yaml
```

my-pod-2.yaml

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-pod-2
spec:
  containers:
  - image: samiche92/my-node-server:1.0.0
    name: my-pod
    ports:
    - containerPort: 8080
    resources:
      requests:
        memory: "100Mi"
        cpu: "3"
      limits:
        memory: "100Mi"
        cpu: "3"
```

```bash
kubectl apply -f my-pod-2.yaml
kubectl describe node minikube | less
kubectl describe pod my-pod-2 | grep QoS
cp my-pod.yaml my-pod-3.yaml 
vim my-pod-3.yaml
# name: my-pod-3
kubectl apply -f my-pod-3.yaml
kubectl get pods
kubectl describe pod my-pod-3.yaml

vim my-lr.yaml
```

my-lr.yaml

```yaml
apiVersion: v1
kind: LimitRange
metadata:
  name: my-lr
spec:
  limits:
  - defaultRequest: # Default requests
      cpu: "1"
    default: # Default limits
      cpu: "2"
    max:
      cpu: "2"
    min:
      cpu: "500m"
    type: Container
```

```bash
kubectl apply -f my-lr.yaml
kubectl run my-pod-4 --image=samiche92/my-node-server:1.0.0 --port=8080
kubectl get pods
kubectl get pod my-pod-4 -o yaml | less

kubectl delete all --all
vim my-rq.yaml
```

my-rq.yaml

```yaml
apiVersion: v1
kind: ResourceQuota
metadata:
  name: my-rq
spec:
  hard:
    cpu: 3
    memory: 2Gi
    pods: 2
```

```bash
kubectl apply -f my-rq.yaml
vim my-lr.yaml
# Ajouter
#  - defaultRequest: # Default requests
#      cpu: "1"
#      memory: "100Mi"
kubectl run my-pod --image=samiche92/my-node-server:1.0.0
kubectl run my-pod-2 --image=samiche92/my-node-server:1.0.0
kubectl run my-pod-3 --image=samiche92/my-node-server:1.0.0
```
