# TP5 - Deployment

```bash
mkdir ../TP05 && cd ../TP05
kubectl delete rc node-server
kubectl delete rs node-server
kubectl create deployment my-app --image=<my-username>/my-node-server:1.0.0 -o yaml --dry-run=client > my-deploy.yaml
vim my-deploy.yaml
```

my-deploy.yaml

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-app
  annotations:
    kubernetes.io/change-cause: "V1"
spec:
  replicas: 2
  selector:
    matchLabels:
      app: backend
  template:
    metadata:
      labels:
        app: backend
    spec:
      containers:
      - image: <my-username>/my-node-server:1.0.0
        name: my-node-server
```

```bash
kubectl apply -f my-deploy.yaml
kubectl get pods
cp ../TP01/app.js .
cp ../TP01/Dockerfile .
vim app.js
# On change la 4e ligne => console.log("Server v2 starting...");
# On change la 9e ligne => response.end("[v2] You've hit "
docker build -t <my-username>/my-node-server:2.0.0 . && docker push <my-username>/my-node-server:2.0.0
vim my-deploy.yaml
# Changer version 1.0.0 => 2.0.0
# Changer kubernetes.io/change-cause: "V1" => kubernetes.io/change-cause: "V2"
kubectl apply -f my-deploy.yaml
kubectl rollout history deployment my-app
mv app.js app-v2.js
vim app.js
```

app.js

```javascript
const http = require('http');
const os = require('os');

var count = 0;

console.log("Server v3 starting...");

var handler = function (request, response) {
  console.log("Received request from " + request.connection.remoteAddress);
  if (request.url == '/repair') {
    count = 0;
    response.writeHead(200);
    response.end("[v3] Server repaired");
    return;
  }
  if (count >= 3) {
    response.writeHead(500);
    response.end("[v3] Some internal error has occurred! Hostname: " + os.hostname() + "\n");
    return;
  }
  count++;
  response.writeHead(200);
  response.end("This is v3 running in " + os.hostname() + "\n");
};

var www = http.createServer(handler);
www.listen(8080);
```

```bash
docker build -t <my-username>/my-node-server:3.0.0 . && docker push <my-username>/my-node-server:3.0.0

vim my-deploy.yaml
# Changer version 2.0.0 => 3.0.0
# Changer annotation kubernetes.io/change-cause: "V2" => kubernetes.io/change-cause: "V3"

kubectl apply -f my-deploy.yaml
kubectl rollout undo deployment my-app --to-revision=2
kubectl rollout history deployment my-app
kubectl delete all --all
```
